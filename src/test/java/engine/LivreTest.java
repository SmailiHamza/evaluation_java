package engine;


import data.Unite;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LivreTest {
    @Test
    public void recherchreRecetteTest() {
        Livre livre = new Livre();
        Auteur chef = new Chef("Cyril", "Lignac", "Le Chardenoux", 1);
        Ingredients ingredients = new Ingredients();
        ingredients.addIngredient("Riz", new Mesure(1, Unite.VERRE));
        ingredients.addIngredient("Potimarron", new Mesure(1, Unite.UNITE));
        ingredients.addIngredient("Boeuf", new Mesure(200, Unite.GRAMME));
        ingredients.addIngredient("Champignons", new Mesure(120, Unite.GRAMME));
        Recette recette = new Recette("Potimarron farci au boeuf haché, riz et champignons", chef, ingredients);
        livre.addRecette(recette);
        Food food1=new Food("Beauf");
        chef.addInMyFridge(food1);
        Recette recetteFind=livre.getRecetteByFridge(chef.getFridge());
        assertEquals("Potimarron farci au boeuf haché, riz et champignons",recette.getNom());
        assertEquals(ingredients,recette.getIngredients());
    }
    @Test
    public void getRecetteTest() {
        Livre livre = new Livre();
        Auteur cuisinier=new Cuisinier("Jean","Imbert","Plaza Athénée");
        Ingredients ingredients=new Ingredients();
        ingredients.addIngredient("Oeuf",new Mesure(2, Unite.UNITE));
        ingredients.addIngredient("Sucre roux",new Mesure(50, Unite.GRAMME));
        ingredients.addIngredient("Beurre salé",new Mesure(100, Unite.GRAMME));
        ingredients.addIngredient("Farine",new Mesure(25, Unite.GRAMME));
        ingredients.addIngredient("Chocolat",new Mesure(200, Unite.GRAMME));
        Recette recette=new Recette("Fondant au chocolat",cuisinier,ingredients);
        livre.addRecette(recette);
        Recette recetteFind=livre.getRecetteByAuteur(cuisinier);
        assertEquals("Fondant au chocolat",recette.getNom());
        assertEquals(recetteFind,livre.getRecetteByAuteur(cuisinier));
    }

}
