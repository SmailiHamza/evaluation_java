package engine;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FridgeTest {
    @Test
    public void existeInTest() {
        Fridge fridge = new Fridge();
        Food food = new Food("Riz");
        fridge.add(food);
        assertEquals(true, fridge.existsIn(food));
    }

    @Test
    public void removeTest() {
        Fridge fridge = new Fridge();
        Food food = new Food("Riz");
        fridge.add(food);
        fridge.remove(food);
        assertEquals(false, fridge.existsIn(food));
    }
}
