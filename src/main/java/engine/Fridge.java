package engine;


import java.util.ArrayList;
import java.util.List;

public class Fridge {
    private List<Food> foods;

    public Fridge() {
        this.foods = new ArrayList<>();
    }

    public int getNbFood() {
        return foods.size();
    }

    public boolean existsIn(Food food) {
        for (Food foodFind : foods) {
            if (food.getName().equalsIgnoreCase(foodFind.getName())) {
                return true;
            }
        }
        return false;
    }

    public void add(Food food) {
        foods.add(food);
    }

    public void remove(Food food) {
        if (existsIn(food)) {
            foods.remove(food);
        }
    }

    public List<Food> getFoods() {
        return foods;
    }

    public String toString() {
        String listOfFood = "";
        if (getNbFood() == 0) {
            return ("Le frigo est vide");
        } else {
            for (Food food : foods) {
                listOfFood += "\n" + food.getName();
            }
            return ("Le contenu du frigo :" + listOfFood);
        }

    }
}
