package engine;

public class Recette {
    private String nom;
    private Auteur auteur;
    private Ingredients ingredients;

    public Recette(String nom, Auteur auteur, Ingredients ingredients) {
        this.nom = nom;
        this.auteur = auteur;
        this.ingredients = ingredients;
    }

    public Auteur getAuteur() {
        return auteur;
    }

    public Ingredients getIngredients() {
        return ingredients;
    }

    public String getNom() {
        return nom;
    }

    public String toString() {
        String recetteDetails = "";
        recetteDetails += "Nom de la recette : " + this.nom + "\nNom de l'auteur : " + auteur.getFirstName() + " " + auteur.getLastName() + "\nListe des ingrédients :\n";
        for (String nom : ingredients.getIngredientList().keySet()) {
            Mesure mesure = ingredients.getIngredientList().get(nom);
            recetteDetails += nom + " : " + mesure.quantité + " " + mesure.unite + "\n";
        }
        return recetteDetails;
    }
}
