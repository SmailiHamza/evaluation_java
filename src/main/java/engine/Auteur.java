package engine;

public interface Auteur {
    public Fridge getFridge();

    public void addInMyFridge(Food food);

    public String getFirstName();

    public String getLastName();
}
