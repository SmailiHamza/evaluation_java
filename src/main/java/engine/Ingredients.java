package engine;

import java.util.HashMap;

public class Ingredients {
    private HashMap<String, Mesure> ingredientList;

    public Ingredients() {
        this.ingredientList = new HashMap<>();
    }

    public void addIngredient(String nom, Mesure mesure) {
        this.ingredientList.put(nom, mesure);
    }

    public HashMap<String, Mesure> getIngredientList() {
        return ingredientList;
    }
}
