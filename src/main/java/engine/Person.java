package engine;


public class Person {
    private String firsteName;
    private String lastName;
    private Fridge fridge;

    public Person(String firsteName, String lastName) {
        this.firsteName = firsteName;
        this.lastName = lastName;
        this.fridge = new Fridge();
    }

    public void addInMyFridge(Food food) {
        fridge.add(food);
    }

    public void removeInMyFridge(Food food) {
        fridge.remove(food);
    }

    public String getFirstName() {
        return firsteName;
    }

    public String getLastName() {
        return lastName;
    }

    public Fridge getFridge() {
        return fridge;
    }
}
