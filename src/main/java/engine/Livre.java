package engine;

import java.util.ArrayList;
import java.util.List;

public class Livre {
    private List<Recette> recetteList;

    public Livre() {
        this.recetteList = new ArrayList<>();
    }

    public boolean existIn(Recette recette) {
        for (Recette recetteFind : recetteList) {
            if (recette.getNom().equalsIgnoreCase(recetteFind.getNom())) {
                return true;
            }
        }
        return false;
    }

    public void addRecette(Recette recette) {
        if (!existIn(recette)) {
            recetteList.add(recette);
        }

    }

    public Recette getRecetteByFridge(Fridge fridge) {
        for (Food food : fridge.getFoods()) {
            for (Recette recette : recetteList) {
                if (recette.getIngredients().getIngredientList().containsKey(food.getName())) {
                    return recette;
                }
            }
        }
        return null;
    }

    public Recette getRecetteByAuteur(Auteur auteur) {

        for (Recette recette : recetteList) {
            if (recette.getAuteur().getFirstName().equalsIgnoreCase(auteur.getFirstName()) && recette.getAuteur().getLastName().equalsIgnoreCase(auteur.getLastName())) {
                return recette;
            }
        }
        return null;
    }
}
