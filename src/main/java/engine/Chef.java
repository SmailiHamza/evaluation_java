package engine;

public class Chef extends Person implements Auteur {

    private String Restaurant;
    private int nbEtoile;

    public Chef(String firsteName, String lastName, String restaurant, int nbEtoile) {
        super(firsteName, lastName);
        this.Restaurant = restaurant;
        this.nbEtoile = nbEtoile;
    }
}
