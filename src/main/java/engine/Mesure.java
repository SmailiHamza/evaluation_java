package engine;

import data.Unite;

public class Mesure {
    public int quantité;
    public Unite unite;

    public Mesure(int quantité, Unite unite) {
        this.quantité = quantité;
        this.unite = unite;
    }
}
