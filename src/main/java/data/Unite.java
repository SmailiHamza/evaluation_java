package data;

public enum Unite {
    UNITE("Unite"),
    GRAMME("Gramme"),
    VERRE("Verre");
    private final String unite;

    Unite(String unite) {
        this.unite=unite;
    }
}
