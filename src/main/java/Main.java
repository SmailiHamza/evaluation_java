import data.Unite;
import engine.*;

public class Main {
    public static void main(String[] args) {
        Person person = new Person("brad", "Pitt");
        System.out.println(person.getFridge().toString());
        Food food = new Food("Riz");
        person.addInMyFridge(food);
        System.out.println(person.getFridge().toString());
        Livre livre = new Livre();
        Auteur chef = new Chef("Cyril", "Lignac", "Le Chardenoux", 1);
        Ingredients ingredients = new Ingredients();
        ingredients.addIngredient("Riz", new Mesure(1, Unite.VERRE));
        ingredients.addIngredient("Potimarron", new Mesure(1, Unite.UNITE));
        ingredients.addIngredient("Boeuf", new Mesure(200, Unite.GRAMME));
        ingredients.addIngredient("Champignons", new Mesure(120, Unite.GRAMME));
        Recette recette = new Recette("Potimarron farci au boeuf haché, riz et champignons", chef, ingredients);
        livre.addRecette(recette);
        System.out.println(livre.getRecetteByFridge(person.getFridge()).getNom());
        System.out.println(livre.getRecetteByFridge(chef.getFridge()));
        Food food1 = new Food("Beauf");
        chef.addInMyFridge(food1);
        System.out.println(livre.getRecetteByFridge(person.getFridge()).getNom());
        Auteur cuisinier = new Cuisinier("Jean", "Imbert", "Plaza Athénée");
        ingredients = new Ingredients();
        ingredients.addIngredient("Oeuf", new Mesure(2, Unite.UNITE));
        ingredients.addIngredient("Sucre roux", new Mesure(50, Unite.GRAMME));
        ingredients.addIngredient("Beurre salé", new Mesure(100, Unite.GRAMME));
        ingredients.addIngredient("Farine", new Mesure(25, Unite.GRAMME));
        ingredients.addIngredient("Chocolat", new Mesure(200, Unite.GRAMME));
        recette = new Recette("Fondant au chocolat", cuisinier, ingredients);
        livre.addRecette(recette);
        System.out.println(livre.getRecetteByAuteur(cuisinier).toString());
        System.out.println(livre.getRecetteByFridge(cuisinier.getFridge()));
        food = new Food("Chocolat");
        cuisinier.addInMyFridge(food);
        System.out.println(livre.getRecetteByFridge(cuisinier.getFridge()));
    }
}
